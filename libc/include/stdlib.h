#ifndef _STDLIB_H
#define _STDLIB_H

#include <sys/cdefs.h>

__BEGIN_DECLS

__attribute((__noreturn__))
void abort(void);

__END_DECLS

#endif // _STDLIB_H
