#ifndef _STDIO_H
#define _STDIO_H

#include <sys/cdefs.h>

__BEGIN_DECLS

int printf(const char* __restrickt, ...);
int putchar(int);
int puts(const char*);

__END_DECLS

#endif // _STDIO_H
