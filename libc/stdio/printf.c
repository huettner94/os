#include <stdbool.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

#if defined(__is_myos_kernel)
#include <kernel/tty.h>
#endif

static void print(const char* data, size_t data_length) {
    for (size_t i = 0; i < data_length; i++) {
        putchar((int) ((const unsigned char*) data)[i]);
    }
}

static size_t printhex(const unsigned int val, const char* chars) {
    size_t start_pos;
    for (start_pos = sizeof(val)*8; start_pos > 0; start_pos -= 4) {
        size_t pos = (val >> (start_pos - 4)) & 0xF;
        if (pos != 0) {
            break;
        }
    }
    size_t written = 0;
    for (; start_pos > 0; start_pos -= 4) {
        size_t pos = (val >> (start_pos - 4)) & 0xF;
        char c = chars[pos];
        print(&c, sizeof(c));
        written+=sizeof(c);
    }
    if (written == 0) {
        char c = '0';
        print(&c, sizeof(c));
        written+=sizeof(c);
    }
    return written;
}

static size_t printpoint(const void* val, const char* chars) {
    size_t v = (size_t) val;
    size_t start_pos;
    for (start_pos = sizeof(v)*8; start_pos > 0; start_pos -= 4) {
        size_t pos = (v >> (start_pos - 4)) & 0xF;
        if (pos != 0) {
            break;
        }
    }
    size_t written = 0;
    for (; start_pos > 0; start_pos -= 4) {
        size_t pos = (v >> (start_pos - 4)) & 0xF;
        char c = chars[pos];
        print(&c, sizeof(c));
        written+=sizeof(c);
    }
    if (written == 0) {
        char c = '0';
        print(&c, sizeof(c));
        written+=sizeof(c);
    }
    return written;
}

int printf(const char* restrict format, ...) {
    va_list parameters;
    va_start(parameters, format);

    int written = 0;
    size_t amount;
    bool rejected_bad_specifier = false;

    while (*format != '\0') {
        if (*format != '%') {
            print_c:
                amount = 1;
                while (format[amount] && format[amount] != '%') {
                    amount++;
                }
                print(format, amount);
                format += amount;
                written += amount;
                continue;
        }

        const char* format_begun_at = format;

        if (*(++format) == '%') {
            goto print_c;
        }

        if (rejected_bad_specifier) {
            incomprehensible_conversion:
                rejected_bad_specifier = true;
                format = format_begun_at;
                goto print_c;
        }

        if (*format == 'c') {
            format++;
            char c = (char) va_arg(parameters, int /* char promotes to int */);
            print(&c, sizeof(c));
            written+=sizeof(c);
        } else if (*format == 's') {
            format++;
            const char* s = va_arg(parameters, const char*);
            size_t len = strlen(s);
            print(s, len);
            written+=len;
        } else if (*format == 'x') {
            format++;
            unsigned int val = va_arg(parameters, unsigned int);
            written+=printhex(val, "0123456789abcdef");
        } else if (*format == 'X') {
            format++;
            unsigned int val = va_arg(parameters, unsigned int);
            written+=printhex(val, "0123456789ABCDEF");
        } else if (*format == 'p') {
            format++;
            void* val = va_arg(parameters, void*);
            written+=printpoint(val, "01234556789abcdef");
        } else {
            goto incomprehensible_conversion;
        }
    }

    va_end(parameters);

    return written;
}
