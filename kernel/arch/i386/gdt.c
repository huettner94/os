#include <stddef.h>
#include <stdint.h>

#include <kernel/gdt.h>
#include <stdio.h>

gdt_entry_t gdt_entries[5];

extern void gdt_flush(uint32_t, size_t);

static void gdt_set_gate(int32_t num, uint32_t base, uint32_t limit, uint16_t flag) {
    gdt_entries[num].base_low = (base & 0xFFFF);
    gdt_entries[num].base_middle = (base >> 16) & 0xFF;
    gdt_entries[num].base_high = (base >> 24) & 0xFF;

    gdt_entries[num].limit_low = (limit & 0xFFFF);
    gdt_entries[num].granularity = (limit >> 16) & 0x0F;

    gdt_entries[num].granularity |= (flag >> 8) & 0xF0;
    gdt_entries[num].access = flag & 0xFF;
}

void gdt_setup() {
    size_t limit = sizeof(gdt_entries) - 1;
    uint32_t base = (uint32_t)&gdt_entries;

    gdt_set_gate(0, 0, 0, 0);
    gdt_set_gate(1, 0, 0xFFFFFFFF, GDT_CODE_PL0);
    gdt_set_gate(2, 0, 0xFFFFFFFF, GDT_DATA_PL0);
    gdt_set_gate(3, 0, 0xFFFFFFFF, GDT_CODE_PL3);
    gdt_set_gate(4, 0, 0xFFFFFFFF, GDT_DATA_PL3);

    gdt_flush(base, limit);

    printf("GDT at: 0x%x\n", (unsigned int) base);
}
