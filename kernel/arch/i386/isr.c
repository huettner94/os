#include <stdint.h>

#include <kernel/isr.h>
#include <stdio.h>

void isr_handler(registers_t* regs) {
    printf("reviced interrupt: 0x%x\n", (unsigned int)regs->int_no);
}
