gdtr:
limit: .word 0 # limit
base: .long 0 # base

.global gdt_flush
.type gdt_flush, @function
gdt_flush:
    movl 4(%esp), %eax
    movl %eax, (base)
    movw 8(%esp), %ax
    movw %ax, (limit)
    lgdt (gdtr)

    # Reload segments
    ljmp $0x08, $reload_CS # 0x08 code selector

reload_CS:
    movw $0x10, %ax # 0x10 data selector
    movw %ax, %ds
    movw %ax, %es
    movw %ax, %fs
    movw %ax, %gs
    movw %ax, %ss
    ret
