#ifndef _KERNEL_ISR_H
#define _KERNEL_ISR_H

#include <stdint.h>

typedef struct registers {
    uint32_t ds;
    uint32_t edi, esi, ebp, esp_current, ebx, edx, ecx, eax; // esp_current -> esp at pusha
    uint32_t int_no, err_code;
    uint32_t eip, es, eflags, esp, ss; // esp -> esp at interrupt
} registers_t;

void isr_handler(registers_t* regs);

#endif // _KERNEL_ISR_H
