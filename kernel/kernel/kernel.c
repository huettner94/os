#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include <stdio.h>

#include <kernel/tty.h>
#include <kernel/multiboot.h>
#include <kernel/gdt.h>
#include <kernel/idt.h>

void kernel_early() {
    terminal_initialize();

    printf("Initializing GDT\n");
    gdt_setup();
    printf("GDT initialized\n");

    printf("Initializing IDT\n");
    idt_setup();
    printf("IDT initialized\n");
}

void kernel_main(multiboot_info_t* mbi) {
    printf("Hello, kernel World!\n");

    printf("Testing Interrupt 0x3\n");
    asm volatile ("int $0x3");
    printf("Testing Interrupt 0x3 complete\n");

    printf("Testing Interrupt 0x4\n");
    asm volatile ("int $0x4");
    printf("Testing Interrupt 0x4 complete\n");

    printf("\nMultibootheader:\n");
    printf("Flags = 0x%x\n", (unsigned) mbi->flags);
    printf("mem_lower = 0x%xKB, mem_upper = 0x%xKB\n", (unsigned) mbi->mem_lower, (unsigned) mbi->mem_upper);
    printf("fb_addr = 0x%p, width = 0x%x, height = 0x%x\n",
        (void*) mbi->framebuffer_addr, (unsigned) mbi->framebuffer_width, (unsigned) mbi->framebuffer_height);
}
